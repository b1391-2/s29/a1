//Creating a Server
const express = require("express");
const app = express();
const PORT = 4000;

//Middle Ware
app.use(express.json());
app.use(express.urlencoded());

//Mock Data Base

let users = [
	{
		username : "calvin",
		password : "calvin123"
	},
	{
		username : "calvin2",
		password : "calvin1231"
	}
];

//ROUTES

//Create a Get Route that will access the /home route that will print out a simple message
//Process a Get request at the /home route using postman

app.get("/home", (request, response) =>{

	console.log(`Working on Get`);
	response.send(`Welcome to the Home Page`);

});

//Create a Get route that will access the /users route that will retrieve all the users in the mock database
//Process a Get request at the /users route using postman

app.get("/users", (request, response) =>{
	console.log(`Working on 2nd Get`);
	console.log(users);
	response.send(users);
})


// Create a Delete route that will access the /delete-user route to remove a user from the mock database
//Process a Delete requrest at the /delete-user route using postman

app.delete("/delete-user", (request, response) =>{
	console.log(`Working on 3rd Delete`);
	console.log(users);
	users.slice();
	console.log(users);
	response.send(`User has been deleted.`)
});

//Listen
app.listen(PORT, () => console.log(`Server running at port : ${PORT}`));